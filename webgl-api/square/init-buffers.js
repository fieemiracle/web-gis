function initBuffers(gl) {
    const positionBuffer = initPositionBuffer(gl);
  
    return {
      position: positionBuffer,
    };
  }
  
  function initPositionBuffer(gl) {
    const positionBuffer = gl.createBuffer();//得到缓冲对象并存储在顶点缓冲器
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);//绑定上下文
  
    const positions = [1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0, -1.0];//创建一个记录正方体每个顶点的数组
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);//将数组转换为webgl浮点类型的数组，并传到gl对象的bufferData()建立对象的顶点
  
    return positionBuffer;
  }
  
  export { initBuffers }; 
  