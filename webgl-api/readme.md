# 模型、视图、投影矩阵
    1、模型矩阵：定义了如何获取原始模型数据并将其在 3D 世界空间中移动

    2、投影矩阵：将世界空间坐标转换为剪裁空间坐标。常用的投影矩阵（透视矩阵）用于模拟充当 3D 虚拟世界中观看者的替身的典型相机的效果

    3、视图矩阵：负责移动场景中的对象以模拟相机位置的变化，改变观察者当前能够看到的内容。
# 关键词
    1、顶点着色器：顶点着色器是GPU渲染管线上一个可以执行着色器语言的功能单元，具体执行的就是顶点着色器程序，WebGL顶点着色器程序在Javascript中以字符串的形式存在，通过编译处理后传递给顶点着色器执行。 
    顶点着色器主要作用就是执行顶点着色器程序对顶点进行变换计算，比如顶点位置坐标执行进行旋转、平移等矩阵变换，变换后新的顶点坐标然后赋值给内置变量gl_Position，作为顶点着色器的输出，图元装配和光栅化环节的输入。

    2、光栅化：片元着色器和顶点着色器一样是GPU渲染管线上一个可以执行着色器程序的功能单元，顶点着色器处理的是逐顶点处理顶点数据，片元着色器是逐片元处理片元数据。通过给内置变量g1_FragClor赋值可以给每一个片元进行着色，值可以是一个确定的RGBA值，可以是一个和片元位置相关的值，也可以是插值后的顶点颜色。除了给片元进行着色之通过关键字discard还可以实现哪些片元可以被丢弃，被丢弃的片元不会出现在帧缓冲区，自然不会显示在canvas画布上。
# 剪裁空间
    1、剪裁空间：在 WebGL 程序中，数据通常上传到具有自己的坐标系统的 GPU 上，然后顶点着色器将这些点转换到一个称为裁剪空间的特殊坐标系上。延展到裁剪空间之外的任何数据都会被剪裁并且不会被渲染。
# 着色器
    （GLSL为OpenGL着色语言，通过用GLSL编写这些着色器，并将代码文本传递给WebGL，使之在 GPU 执行时编译。顶点着色器和片段着色器的集合我们通常称为着色器程序）
    1、携带着绘制形状的顶点信息以及构造绘制在屏幕上像素的所需数据，换句话说，它负责记录着像素点的位置和颜色

    2、顶点着色器：每次渲染一个形状时，顶点着色器会在形状中的每个顶点运行。工作：将输入顶点从原始坐标系转换到WebGL使用的裁剪空间坐标系，其中每个轴的坐标范围从 -1.0 到 1.0，并且不考虑纵横比，实际尺寸或任何其他因素。
    顶点着色器需要对顶点坐标进行必要的转换，在每个顶点基础上进行其他调整或计算，然后通过将其保存在由 GLSL 提供的特殊变量（我们称为 gl_Position）中来返回变换后的顶点
    顶点着色器根据需要，也可以完成其他工作。例如，决定哪个包含 texel (en-US) 面部纹理的坐标，可以应用于顶点；通过法线来确定应用到顶点的光照因子等。然后将这些信息存储在变量（varyings)或属性 (attributes)属性中，以便与片段着色器共享

    3、片段着色器：
        片段着色器在顶点着色器处理完图形的顶点后，会被要绘制的每个图形的每个像素点调用一次。它的职责是确定像素的颜色，通过指定应用到像素的纹理元素（也就是图形纹理中的像素），获取纹理元素的颜色，然后将适当的光照应用于颜色。之后颜色存储在特殊变量 gl_FragColor 中，返回到 WebGL 层。该颜色将最终绘制到屏幕上图形对应像素的对应位置。
