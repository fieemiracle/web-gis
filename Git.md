# 本地
# 一、git分支（git branch） --基于某个提交以及它所有的提交进行新的工作
    1、创建一个名字为newBranch的分支  git branch newBranch
    2、查看当前分支 git branch
    3、切换到指定分支,再提交代码 git checkout <name>，git commit
    （在GIT 2.23版本中，引入了名字为git switch新命令，最终会取代git checkout;因为git checkout作为单个命令有点超载（它承载了很多独立的功能））
    4、创建一个新的分支同时切换到新创建的分支上  git checkout -b <your-branch-name>

# 二、git合并（git merge）  --将两个分支合并到一起（新建一个分支，在这个分支上开发新的功能，开发完成后再合并回主线（主分支master））
    --在git合并两个分支时会产生一个特殊的提交记录，他有两个父节点
    1、把分支newBranch合并到主分支master里 --git merge newBranch
    2、再把master分支合并到newBranch --git checkout newBranch，git merge master（因为master继承自newBranch）

    综上所述:
    (1)创建新分支newBranch  --git branch newBranch
    (2)切换到该分支  --git checkout newBranch
    (3)提交一次 --git commit
    (4)切换到主分支  --git checkout master
    (5)再提交一次 --git commit
    (6)将分支合并到主分支 --git merge newBranch

# 三、合并分支（git rebase）--取出一系列的提交记录，复制“它们”，然后在另外一个地方逐个放下去
    --git rebase优点是：可以创造更线性的提交历史，也就是代码库的提交历史将会变得异常清晰
    （前提：准备两个分支--newBranch和master，把newBranch分支的工作直接移到master分支上，移动后会使得两个分支的功能看起来像是按顺序开发，但实际上他们是并行开发的
    1、新建分支newBranch并切换到newBranch分支  --git checkout -b newBranch（相当于git branch newBranch+git checkout newBranch）
    2、提交一次 --git commit
    3、切换回master分支再提交一次 --git checkout master,git commit
    4、再次切换回newBranch分支，rebase到master上  --git rebase newBranch

# 四、分离HEAD --学习在提交树上前后移动的几种方法。HEAD是对当前分支的符号引用，指向你正在其基础上进行工作的提交记录。HEAD总是指向当前分支最近一次提交记录，大多数修改提交树的Git命令都是从改变HEAD的指向开始的，HEAD通常情况下指向分支名的，改变了该分支的状态，这个变化可以通过HEAD变得可见
    （要求：查看提交前后HEAD的位置）
    --git checkout newBranch，git checkout master，git commit，git checkout otherBranch（实际上这些命令并不是真的在查看HEAD的指向）
    --查看HEAD的指向  --cat .git/HEAD
    --如果HEAD的指向是一个引用的话  --git symbolic-ref HEAD
    （分离的HEAD就是让其指向了某个具体的提交记录而不是分支名）

    我们可以：
    （1）从newBranch分支中分离出HEAD并且让其指向一个提交记录（通过哈希值指定提交记录，每个提交记录的哈希值显示在代表提交记录的圆圈中）

# 五、相对引用 (^)
    1、相对引用的简单用法：
    （1）使用 * 向上移动一个提交记录
    （2）使用 -<num> 向上移动多个提交记录，例如 -3
    2、查看操作符 ^ --把这个符号加在引用名称的后面，表示让GIT寻找指定提交记录的父提交，
        main^ === main的父节点
        main^^ === main的第二个父节点
        切换到main的父节点  --git checkout main^

# 六、相对引用（~） 一个 ^ 就代表在提交树上移动几步，而 ~ 该操作符后面可以更一个数字（该数字可选，不跟数字与 ^ 相同，向上移动一次），表示向上移动多少次
    1、一次性向上移动4步，即后退四步 --git checkout  HEAD~4
    2、强制修改分支位置 --可以直接使用 -f 选项让分支指向另外一个提交 --> git branch -f master HEAD~3（该命令会强制将 master 分支强制指向 HEAD 的第3父提交）

# 七、撤销变更 --和提交一样，撤销变更由底层部分（暂存区的独立文件或者片段）和上层部分（变更到底是通过哪种方式被撤销的）组成的
    --主要有两种方式用来撤销变更： git reset  和 git revert 
    git reset 通过把分支记录回退几个提交记录来实现撤销改动，git reset 向上移动分支，原来指向的提交记录就跟从来没有提交一样（类似改写历史）
    git revert 可以撤销更改并分享给别人
    1、把分支移动回到上一次提交，这样本地代码库就不知道有本次提交，对远程分支无效 --git reset HEAD~1
    2、同样把分支移动到上一次提交 -- git revert HEAD（可以把更改推送到远程仓库与别人分享）

# 八、整理提交记录
    1、如果你想将一些提交复制到当前所在的位置（HEAD）下面的话，Cherry-pick是最直接的方法，前提是知道所需的提交记录和这些提交记录的哈希值
        git cherry-pick <提交号>...

    2、如果不清除想要的提交记录的哈希值，可以利用交互式的rebase，可以从一系列的提交记录中找到想要的记录
        交互式 rebase 指的是使用带参数 --interactive 的 rebase 命令，简写 -i
        如果在命令后增加了这个选项，Git会打开一个UI界面并列出将要被复制到目标分支的备选提交记录，还会显示每个提交记录的哈希值和提交说明，提交说明有助于理解这个提交进行了哪些更改
        在实际使用时，所谓的UI窗口一般会在文本编辑器 --如Vim --中打开一个文件，当rebase UI 界面打开时，可以做3件事情：
        （1）调整提交记录的顺序（通过鼠标拖放来完成）
        （2）删除你不想要的提交（通过切换 pick 的状态完成，关闭就意味着你不想要这个提交记录）
        （3）合并提交。遗憾的是由于某种逻辑的原因，我们的课程不支持此功能，反正它允许你把多个提交记录合并成一个
            git rebase -i HEAD~4

# 九、本地栈式提交 

# 远程
# 一、git clone --在真实的环境下的作用是在本地创建一个远程仓库的拷贝
# 二、远程分支 
    远程分支的命名规范 <remote name>/<branch name>，当git clone 某个仓库时，Git已经帮我们吧远程仓库的名称设置为 origin 了
    通过 git checkout origin/master 切换到远程分支，再git commit。origin/master只有在远程仓库中相应的分支更新以后才会更新

# 三、git fetch --从远程仓库获取数据
    Git 远程仓库相当的操作实际上可以分为两个点：一，向远程仓库传输诉苦以及从远程仓库获取数据；二，与远程仓库同步后可以分享任何可以被 git 管理的更新
    git fetch 做了什么？
    （1）从远程仓库下载本地仓库中确实的提交记录
    （2）更新远程分支指针（如origin/master）
    实际上将本地仓库中的远程分支更新成了远程仓库相应分支最新的状态
    git fetch 不会做什么？
    （1）不会改变本地仓库的状态，也不会更新 master 分支，更不会修改磁盘上的文件
    可以理解成单纯的下载操作

# 四、git pull --先抓取更新再合并到分支
    当远程分支有新的提交时，可以像本地分支那样合并远程分支，即：
        git cherry-pick origin/master
        git rebase origin/master
        git merge origin/master
    实际上，git pull == git fetch + git merge <branch name>